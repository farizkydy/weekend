import React from "react"
import { Navbar } from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import "../assets/styles/footer.scss"

const Footer = (props) => {;
  
    return (
      <React.Fragment>
      <div className="footer">
        <Navbar color="faded" light>
          <div className="footer-weekend">
              <p><span>wknd@</span>2020</p>
          </div>
          <div className="footer-alpha">
              <p>alpha version 0.1</p>
          </div>
        </Navbar>
      </div>
      </React.Fragment>
    );
  }
  
  export default Footer;