import axios from "axios"

export const getTips = () => async dispatch => {
    dispatch({
        type: "GET_TIPS"
    })
    try {
        const res = await axios.get("https://wknd-take-home-challenge-api.herokuapp.com/help-tips")
        dispatch({
            type:"GET_TIPS",
            payload: res.data
        })
    } catch(error) {
        console.log(error)
    }
}