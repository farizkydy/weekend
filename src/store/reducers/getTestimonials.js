const initialState   = {
    testimonials: []
}

const getTestimonials = (state = initialState, action) => {
    const {type, payload} = action
    switch (type) {
        default:
            return{
                ...state
            }
        case "GET_TESTIMONIALS":
        return{
            ...state,
            testimonials:payload
        }
    }
}

export default getTestimonials;