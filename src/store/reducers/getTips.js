const initialState   = {
    tips: []
}

const getTips = (state = initialState, action) => {
    const {type, payload} = action
    switch (type) {
        default:
            return{
                ...state
            }
        case "GET_TIPS":
        return{
            ...state,
            tips:payload
        }
    }
}

export default getTips;